#pragma once

//cpg
#define numInputs 2
#define numOutputs 1
#define numRows 1
#define numColumns 20
#define numNodeInputs 2 
#define numFunctions 9

//evolutionBobAndEve
#define generations 50
#define populationSize 10
#define tournamentSize 5
#define crossoversInGeneration 3
#define mutationProbability 0.5

//evolutionAlice
#define generationsA 100
#define populationSizeA 50
#define tournamentSizeA 10
#define crossoversInGenerationA 8
#define mutationProbabilityA 0.5

//key
#define keyLength 4

//number of plaintexts and key
#define sizeLearningSet 5
#define sizeTestingSet 50
